#include "Camera.h"
#include <glm/gtx/transform2.hpp>

// Default camera values
const float DEF_FOV = 45.0f;

//---------------------------------------------------
// Abstract Camera
//---------------------------------------------------

Camera::Camera()
    :mPosition(glm::vec3(0.0f, 0.0f, 0.0f)),
    mTargetPos(glm::vec3(0.0f, 0.0f, 0.0f)),
    mUp(0.0f, 1.0f, 0.0f),
    mRight(0.0f, 0.0f, 0.0f),
    WORLD_UP(0.0f, 1.0f, 0.0f),
    mYaw(glm::pi<float>()),
    mPitch(0.0f),
    mFOV(DEF_FOV)
{}

glm::mat4 Camera::getViewMatrix() const
{
    return glm::lookAt(mPosition, mTargetPos, mUp);
}

const glm::vec3& Camera::getLook() const
{
    return mLook;
}

const glm::vec3& Camera::getRight() const
{
    return mRight;
}

const glm::vec3& Camera::getUp() const
{
    return mUp;
}

const glm::vec3& Camera::getPosition() const
{
    return mPosition;
}

//---------------------------------------------------
// FPSCamera
//---------------------------------------------------

// Constructor
FPSCamera::FPSCamera(glm::vec3 position, float yaw, float pitch)
{
    mPosition = position;
    mYaw = yaw;
    mPitch = pitch;
}

// Set the camera's position
void FPSCamera::setPosition(const glm::vec3& position)
{
    mPosition = position;
}

// Move the camera's position
void FPSCamera::move(const glm::vec3& offsetPos)
{
    mPosition += offsetPos;
    updateCameraVectors();
}

// Rotate camera using Yaw and Pitch angles passed in degrees
void FPSCamera::rotate(float yaw, float pitch)
{
    mYaw += glm::radians(yaw);
    mPitch += glm::radians(pitch);

    // Constrain the pitch
    mPitch = glm::clamp(mPitch, -glm::pi<float>() / 2.0f + 0.1f, glm::pi<float>() / 2.0f - 0.1f);
    updateCameraVectors();
}

// Calculates the vectors from the Camera's (updated) Euler Angles
void FPSCamera::updateCameraVectors()
{
    // Spheraical to Cartesian coordinates
    // https://en.wikipedia.org/wiki/Spherical_coordinate_system (Note: Our coordinate sys has Y up not Z)

    // Calculate the view direction vector based on yaw and pitch angles (roll not considered)
    glm::vec3 look;
    look.x = cosf(mPitch) * sinf(mYaw);
    look.y = sinf(mPitch);
    look.z = cosf(mPitch) * cosf(mYaw);

    mLook = glm::normalize(look);

    // Re-calculate the Right and Up vector. For simplicity the Right vector will
    // be assumed horizontal w.r.t. the world's Up vector.
    mRight = glm::normalize(glm::cross(mLook, WORLD_UP));
    mUp = glm::normalize(glm::cross(mRight, mLook));

    mTargetPos = mPosition + mLook;
}

//---------------------------------------------------
// FPSCamera
//---------------------------------------------------

// Constructor
OrbitCamera::OrbitCamera()
    : mRadius(10.0f)
{}

// Set the target position
void OrbitCamera::setLookAt(const glm::vec3& target)
{
    mTargetPos = target;
}

// Set the radial distance from the target position to the camera
void OrbitCamera::setRadius(float radius)
{
    mRadius = glm::clamp(radius, 2.0f, 80.0f);
}

// Rotate the camera around target position
void OrbitCamera::rotate(float yaw, float pitch)
{
    mYaw = glm::radians(yaw);
    mPitch = glm::radians(pitch);

    mPitch = glm::clamp(mPitch, - glm::pi<float>() / 2.0f + 0.1f, glm::pi<float>() / 2.0f - 0.1f);

    updateCameraVectors();
}

// Calculate the front vector from the Camera's (updated) Euler Angles
void OrbitCamera::updateCameraVectors()
{
    // Spheraical to Cartesian coordinates
    // https://en.wikipedia.org/wiki/Spherical_coordinate_system (Note: Our coordinate sys has Y up not Z)
    mPosition.x = mTargetPos.x + mRadius * cosf(mPitch) * sinf(mYaw);
    mPosition.y = mTargetPos.y + mRadius * sinf(mPitch);
    mPosition.z = mTargetPos.z + mRadius * cos(mPitch) * cosf(mYaw);
}
