#include <iostream>
#include <sstream>
#include <string>

#define GLEW_STATIC
#include "GL/glew.h" // Important - this header must come before glfw3 header
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "ShaderProgram.h"
#include "Texture2D.h"
#include "Camera.h"
#include "Mesh.h"

// Global Variables
const char* APP_TITLE = "Introduction to Modern OpenGL";
int gWindowWidth = 1024;
int gWindowHeight = 768;

bool gFullscreen = false;
GLFWwindow* gWindow = NULL;
bool gWireframe = false;
bool gFlashlightOn = true;

// Camera
FPSCamera fpsCamera(glm::vec3(0.0f, 3.0f, 10.0f));
const double ZOOM_SENSITIVITY = -3.0;
const float MOVE_SPEED = 5.0; // units per second
const float MOUSE_SENSITIVITY = 0.1f;

// Function prototypes
void glfw_onKey(GLFWwindow* window, int key, int scancode, int action, int mode);
void glfw_OnFrameBufferSize(GLFWwindow* window, int width, int height);
void glfw_onMouseMove(GLFWwindow* window, double posX, double posY);
void glfw_onMouseScroll(GLFWwindow* window, double deltaX, double deltaY);
void update(double elapsedTime);
void ShowFPS(GLFWwindow* window);
bool initOpenGL();

//-----------------------------------------------------------------------------
// Main Application Entry Point
//-----------------------------------------------------------------------------
int main()
{
    if (!initOpenGL())
    {
        // An error occured
        std::cerr << "GLFW initialization failed" << std::endl;
        return -1;
    }

    ShaderProgram lightShader;
    lightShader.loadShaders("shaders/basic.vert", "shaders/basic.frag");

    ShaderProgram lightingShader;
    lightingShader.loadShaders("shaders/lighting_spot.vert", "shaders/lighting_spot.frag");

    // Load meshes and textures
    const int numModels = 7;
    Mesh mesh[numModels];
    Texture2D texture[numModels];

    // Model Positions
    glm::vec3 modelPos[] = {
        glm::vec3(-2.5f, 1.0f, 0.0f),  // crate1
        glm::vec3(2.5f,  1.0f, 0.0f),  // crate2
        glm::vec3(0.0f,  0.0f, -2.0f), // robot
        glm::vec3(0.0f,  0.0f, 0.0f),  // floor
        glm::vec3(0.0f, 0.0f, 2.0f),   // pin
        glm::vec3(-2.0f, 0.0f, 2.0f),  // bunny
        glm::vec3(-5.0f, 0.0f, 0.0f)   // lamb post
    };

    // Model scale
    glm::vec3 modelScale[] = {
        glm::vec3(1.0f, 1.0f, 1.0f),    // crate1
        glm::vec3(1.0f, 1.0f, 1.0f),    // crate2
        glm::vec3(1.0f, 1.0f, 1.0f),    // robot
        glm::vec3(10.0f, 1.0f, 10.0f),  // floor
        glm::vec3(0.1f, 0.1f, 0.1f),    // pin
        glm::vec3(0.7f, 0.7f, 0.7f),    // bunny
        glm::vec3(1.0f, 1.0f, 1.0f)     // lamb post
    };

    mesh[0].loadOBJ("models/barrel.obj");
    mesh[1].loadOBJ("models/woodcrate.obj");
    mesh[2].loadOBJ("models/robot.obj");
    mesh[3].loadOBJ("models/floor.obj");
    mesh[4].loadOBJ("models/bowling_pin.obj");
    mesh[5].loadOBJ("models/bunny.obj");
    mesh[6].loadOBJ("models/lampPost.obj");

    texture[0].loadTexture("textures/barrel_diffuse.png", true);
    texture[1].loadTexture("textures/woodcrate_diffuse.jpg", true);
    texture[2].loadTexture("textures/robot_diffuse.jpg", true);
    texture[3].loadTexture("textures/tile_floor.jpg", true);
    texture[4].loadTexture("textures/AMF.tga", true);
    texture[5].loadTexture("textures/bunny_diffuse.jpg", true);
    texture[6].loadTexture("textures/lamp_post_diffuse.png", true);
    
    double lastTime = glfwGetTime();
    float angle = 0.0f;

    // Rendering loop
    while (!glfwWindowShouldClose(gWindow))
    {
        ShowFPS(gWindow);

        double currentTime = glfwGetTime();
        double deltaTime = currentTime - lastTime;

        // Poll for and process events
        glfwPollEvents(); // Query the window to any keyboard and mouse event
        update(deltaTime); // Call method for handling input changes 

        // Clear the screen & Depth buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 model, view, projection;

        // Create the View matrix
        view = fpsCamera.getViewMatrix();

        // Create the projection matrix
        projection = glm::perspective(glm::radians(fpsCamera.getFOV()), (float)gWindowWidth / (float)gWindowHeight, 0.1f, 200.0f);

        glm::vec3 viewPos;
        viewPos.x = fpsCamera.getPosition().x;
        viewPos.y = fpsCamera.getPosition().y;
        viewPos.z = fpsCamera.getPosition().z;

        // the light
        glm::vec3 lightPos = fpsCamera.getPosition();
        const glm::vec3 lightColor(1.0f, 1.0f, 1.0f);
        lightPos.y -= 0.5f;

        // Must be called BEFORE setting uniforms because setting uniforms is done
        lightingShader.use();
        
        // Set Uniforms - Pass the matrices to the shader
        lightingShader.setUniform("view", view);
        lightingShader.setUniform("projection", projection);
        lightingShader.setUniform("viewPos", viewPos);
        
        // Point light
        lightingShader.setUniform("light.ambient", glm::vec3(0.2f, 0.2f, 0.2f));
        lightingShader.setUniform("light.diffuse", lightColor);
        lightingShader.setUniform("light.specular", glm::vec3(1.0f, 1.0f, 1.0f));
        lightingShader.setUniform("light.position", lightPos);
        lightingShader.setUniform("light.direction", fpsCamera.getLook()); // Spot Light Direction
        lightingShader.setUniform("light.constant", 1.0f); // from point light table
        lightingShader.setUniform("light.linear", 0.07f); // from point light table
        lightingShader.setUniform("light.exponent", 0.017f); // from point light table
        lightingShader.setUniform("light.cosInnerCone", glm::cos(glm::radians(15.0f))); 
        lightingShader.setUniform("light.cosOuterCone", glm::cos(glm::radians(20.0f))); 
        lightingShader.setUniform("light.on", gFlashlightOn); 

        // Render the scene
        for (int i = 0; i < numModels; i++)
        {
            model = glm::translate(glm::mat4(), modelPos[i]) * glm::scale(glm::mat4(), modelScale[i]);
            lightingShader.setUniform("model", model);

            lightingShader.setUniform("material.ambient", glm::vec3(0.1f, 0.1f, 0.1f));
            lightingShader.setUniformSampler("material.diffuseMap", 0);
            lightingShader.setUniform("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
            lightingShader.setUniform("material.shininess", 32.0f);


            texture[i].bind(0); // set the texture before drawing.  Our simple OBJ mesh loader does not do materials yet.
            mesh[i].draw();     // Render the OBJ mesh
            texture[i].unBind(0);
        }

        // Swap front and back buffers
        glfwSwapBuffers(gWindow); // Double buffer

        lastTime = currentTime;
    }

    glfwTerminate();
    return 0;
}

bool initOpenGL()
{
    if (!glfwInit())
    {
        std::cerr << "GLFW initialization failed" << std::endl;
        return false;
    }

    //glEnable(GL_BLEND);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    if (gFullscreen)
    {
        GLFWmonitor* pMonitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* pVmode = glfwGetVideoMode(pMonitor);
        if (pVmode != NULL)
        {
            gWindow = glfwCreateWindow(pVmode->width, pVmode->height, APP_TITLE, pMonitor, NULL);
        }
    }
    else
    {
        gWindow = glfwCreateWindow(gWindowWidth, gWindowHeight, APP_TITLE, NULL, NULL);
    }
    if (gWindow == NULL)
    {
        std::cerr << "Failed to create GLFW windows" << std::endl;
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(gWindow);

    glfwSetKeyCallback(gWindow, glfw_onKey); // Using Keyboard & Mouse input
    glfwSetCursorPosCallback(gWindow, glfw_onMouseMove);
    glfwSetScrollCallback(gWindow, glfw_onMouseScroll);

    // Hide and grabs cursor, unlimited movement
    glfwSetInputMode(gWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPos(gWindow, gWindowWidth / 2.0, gWindowHeight / 2.0);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        std::cerr << "GLEW initialization failed" << std::endl;
        return false;
    }

    glViewport(0, 0, gWindowWidth, gWindowHeight);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.23f, 0.38f, 0.47f, 1.0f);

    return true;
}

void glfw_onKey(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if (key == GLFW_KEY_Q && action == GLFW_PRESS)
    {
        gWireframe = !gWireframe;
        if (gWireframe)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    if (key == GLFW_KEY_F && action == GLFW_PRESS)
        gFlashlightOn = !gFlashlightOn;
}


void glfw_OnFrameBufferSize(GLFWwindow* window, int width, int height)
{
    gWindowWidth = width;
    gWindowHeight = height;
    glViewport(0, 0, gWindowWidth, gWindowHeight);
}

void glfw_onMouseMove(GLFWwindow* window, double posX, double posY)
{
    /*static glm::vec2 lastMousePos = glm::vec2(0, 0);

    // Update angles based on Left Mouse Button input to orbit around the cube
    if (glfwGetMouseButton(gWindow, GLFW_MOUSE_BUTTON_LEFT) == 1)
    {
        // each pixel represents a quarter of a degree rotation (this is out mouse sensitivity)
        gYaw -= ((float)posX - lastMousePos.x) * MOUSE_SENSITIVITY;
        gPitch += ((float)posY - lastMousePos.y) * MOUSE_SENSITIVITY;
    }

    // Change orbit camera radius with Right Mouse Button
    if (glfwGetMouseButton(gWindow, GLFW_MOUSE_BUTTON_RIGHT == 1))
    {
        float dx = 0.01f * ((float)posX - lastMousePos.x);
        float dy = 0.01f * ((float)posY - lastMousePos.y);
        gRadius += dx - dy;
    }

    lastMousePos.x = (float)posX;
    lastMousePos.y = (float)posY;*/
}


// Called by GLFW when the mouse wheel is rotated
void glfw_onMouseScroll(GLFWwindow* window, double deltaX, double deltaY)
{
    double fov = fpsCamera.getFOV() + deltaY * ZOOM_SENSITIVITY;

    fov = glm::clamp(fov, 1.0, 120.0);

    fpsCamera.setFOV((float)fov);
}

// Update stuff every frame
void update(double elapsedTime)
{
    // Camera orientation
    double mouseX, mouseY;

    // Get the current mouse cursor position delta
    glfwGetCursorPos(gWindow, &mouseX, &mouseY);

    // Rotate the camera the difference in mouse distance from the center screen. Multiply this delta by a speed scaler
    fpsCamera.rotate((float)(gWindowWidth / 2.0 - mouseX) * MOUSE_SENSITIVITY, (float)(gWindowHeight / 2.0 - mouseY) * MOUSE_SENSITIVITY);

    // Clamp mouse cursor to center of screen
    glfwSetCursorPos(gWindow, gWindowWidth / 2.0, gWindowHeight / 2.0);

    // Camera FPS movement

    // Forward / Backward
    if (glfwGetKey(gWindow, GLFW_KEY_W) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * fpsCamera.getLook());
    else if (glfwGetKey(gWindow, GLFW_KEY_S) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * -fpsCamera.getLook());
    // Strade Left / Right
    if (glfwGetKey(gWindow, GLFW_KEY_A) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * -fpsCamera.getRight());
    else if (glfwGetKey(gWindow, GLFW_KEY_D) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * fpsCamera.getRight());
    // Up / Down
    if (glfwGetKey(gWindow, GLFW_KEY_Z) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * fpsCamera.getUp());
    else if (glfwGetKey(gWindow, GLFW_KEY_X) == GLFW_PRESS)
        fpsCamera.move(MOVE_SPEED * (float)elapsedTime * -fpsCamera.getUp());
}

void ShowFPS(GLFWwindow* window)
{
    static double previousSecond = 0.0;
    static int frameCount = 0;
    double elapsedSeconds;
    double currentSeconds = glfwGetTime(); // returns number if seconds since GLFW started, as a double

    elapsedSeconds = currentSeconds - previousSecond;

    //limit text update 4 times per second
    if (elapsedSeconds > 0.25)
    {
        previousSecond = currentSeconds;
        double fbs = (double)frameCount / elapsedSeconds;
        double msPerFrame = 1000.0 / fbs;

        std::ostringstream outs;
        outs.precision(3);
        outs << std::fixed
            << APP_TITLE << "  "
            << "FPS:" << fbs << "  "
            << "Frame Time: " << msPerFrame << " (ms)";
        glfwSetWindowTitle(window, outs.str().c_str());

        frameCount = 0;
    }

    frameCount++;
}

